'use strict';

var requireDir = require('require-dir');
requireDir('./gulp/tasks', {recurse: true});

var gulp = require('gulp');

gulp.task('clean', gulp.parallel('clean:email'));

gulp.task('build', gulp.parallel('build:email'));

gulp.task('watch', gulp.parallel('watch:email'));

gulp.task('serve', gulp.parallel('serve:email'));

gulp.task('serve', gulp.series('clean', 'build', 'serve', 'watch'));
