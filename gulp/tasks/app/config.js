'use strict';

var config = {
  source: {
    root: './src'
  },
  watch: {
    root: './src'
  },
  dist: {
    root: './dist'
  },
  email: {
    domain: 'myitworksnews.com',
    category: 'test',
    name: 'email-igniter',
    title: 'Email Igniter 4.1',
    trigger: {
      intl: 'intlTrigger',
      customerTypes: 'customerTypesTrigger',
      remoteFile: 'remoteFileTrigger',
      title: 'titleTrigger'
    },
    production: false,
    video: true,
    intl: {
      active: true,
      folder: 'intl',
      versions: ['au', 'eu', 'ca']
    },
    customerTypes: {
      active: false,
      folder: 'customer-type',
      versions: ['dt', 'lc']
    }
  },
  port: 2000
};

//email paths
config.email.remoteFolder = 'http://www.' + config.email.domain + '/e-newsletters/' + config.email.category + '/' + config.email.name;
config.email.remotePath = config.email.remoteFolder + '/' +  config.email.name;
config.email.assetPath = config.email.remoteFolder + '/assets/img/';

// source files
config.source.css = config.source.root + '/scss/main.scss';
config.source.html = config.source.root + '/*.html';
config.source.images = config.source.root + '/img/**/*';
config.source.partials = config.source.root + '/partials';

// files to watch
config.watch.css = config.watch.root + '/scss/**/*.scss';
config.watch.html = config.watch.root + '/**/*.html';
config.watch.images = config.watch.root + '/img/**/*.*';

// distribution folders
config.dist.css = config.dist.root + '/assets/css';
config.dist.html = config.dist.root + '/';
config.dist.images = config.dist.root + '/assets/img';
config.dist.serve = config.dist.root + '/' + config.email.name + '.html';

module.exports = config;
