'use strict';

var gulp = require('gulp');
var images = require('./../common/images');
var inline = require('./../common/inline');
var css = require('./../common/css');
var html = require('./../common/html');
var serve = require('./../common/serve');
var versions = require('./../common/versions');
var video = require('./../common/video');
var replaceLinks = require('./../common/replace-links');
var replaceCharacters = require('./../common/replace-characters');

var config = require('./config.js');

gulp.task('clean:email-images', images(config).clean);
gulp.task('build:email-images', images(config).build);

gulp.task('clean:email-html', html(config).clean);
gulp.task('build:email-html', html(config).build);

gulp.task('clean:email-css', css(config).clean);
gulp.task('build:email-css', css(config).build);

gulp.task('build:email-inline', inline(config).build);

gulp.task('build:email-versions', versions(config).build);

gulp.task('build:email-video', video(config).build);

gulp.task('build:email-replace-links', replaceLinks(config).build);

gulp.task('build:email-replace-characters', replaceCharacters(config).build);

gulp.task('clean:email', gulp.series(
  'clean:email-images',
  'clean:email-css',
  'clean:email-html'
));

gulp.task('build:email', gulp.series(
  'build:email-html',
  'build:email-css',
  'build:email-inline',
  'build:email-video',
  'build:email-images',
  'build:email-replace-links',
  'build:email-versions',
  'build:email-replace-characters'
));

gulp.task('watch:email', function() {
    gulp.watch(config.watch.css, gulp.series('clean', 'build'));
    gulp.watch(config.watch.html, gulp.series('clean', 'build'));
    gulp.watch(config.watch.images, gulp.series('clean', 'build'));
});

gulp.task('serve:email', serve(config).open);
