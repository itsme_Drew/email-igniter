'use strict';

var gulp = require('gulp');
var del = require('del');
var replace = require('gulp-replace');
var inject = require('gulp-inject');
var rename = require('gulp-rename');
var series = require('stream-series');
var notify = require('gulp-notify');

var html = function(config) {
    var self = {};

    self.clean = function() {
        return del(config.dist.html);
    };

    self.build = function() {
      var preheader = gulp.src([config.source.partials + '/preheader.html']);
      var nav = gulp.src([config.source.partials + '/nav.html']);
      var header = gulp.src([config.source.partials + '/header.html']);
      var body = gulp.src([config.source.partials + '/body.html']);
      var footer = gulp.src([config.source.partials + '/footer.html']);

      return gulp.src(config.source.html)
          .pipe(inject(series(preheader, nav, header, body, footer), {
            starttag: '<!-- {{name}}:partials -->',
            endtag: '<!-- end {{name}}:partials -->',
            transform: function (filePath, file) {
              return file.contents.toString('utf8')
            }
          }))
          .pipe(replace(config.email.trigger.title, config.email.title))
          .pipe(rename(config.email.name + '.html'))
          .pipe(gulp.dest(config.dist.html));
    };


    self.cleanBuild = function() {
        self.clean();
        self.build();
    };

    self.watch = function() {
        gulp.watch(config.watch.html, self.build);
    };

    return self;
};

module.exports = html;
