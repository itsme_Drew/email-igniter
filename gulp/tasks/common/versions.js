'use strict';

var gulp = require('gulp');
var del = require('del');
var replace = require('gulp-replace');
var inject = require('gulp-inject');
var rename = require('gulp-rename');
var series = require('stream-series');
var notify = require('gulp-notify');
var inlineCss = require('gulp-inline-css');
var inlinesource = require('gulp-inline-source');


var versions = function(config) {
    var self = {};
    var email = config.email;
    var _customerTypes = email.customerTypes;
    var _intl = email.intl;

    self.build = function(done) {
      if (_customerTypes.active) {
        for (var i = 0; i < _customerTypes.versions.length; i++) {
          self.buildCustomVersion(_customerTypes.folder, _customerTypes.versions[i], email.trigger.customerTypes);
        }
      }
      else if (_intl.active) {
        for (var i = 0; i < _intl.versions.length; i++) {
          self.buildCustomVersion(_intl.folder, _intl.versions[i], email.trigger.intl);
        }
      }
      done();
    };

    self.buildCustomVersion = function(folder, customType, trigger) {
      return gulp.src(config.dist.serve)
          .pipe(replace(trigger, customType))
          .pipe(inject(series(gulp.src([config.source.partials + '/' + folder + '/' + customType + '/*.html'])), {
            starttag: '<!-- {{name}}:partials:' + customType + ' -->',
            endtag: '<!-- end {{name}}:partials:' + customType + ' -->',
              transform: function (filePath, file) {
              return file.contents.toString('utf8')
            }
          }))
          .pipe(replace(email.remotePath, email.remotePath + '-' + customType))
          .pipe(replace(email.trigger.remoteFile, email.remotePath + '-' + customType + '.html'))
          .pipe(inlineCss({
            removeStyleTags: false
          }))
          .pipe(rename({ suffix: '-' + customType }))
          .pipe(gulp.dest(config.dist.html))
          .pipe(notify('BUILT CUSTOM ' + customType.toUpperCase()));
    };

    return self;
};

module.exports = versions;
