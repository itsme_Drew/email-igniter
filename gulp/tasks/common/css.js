'use strict';

var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var inlineCss = require('gulp-inline-css');
var inlinesource = require('gulp-inline-source');
var minifycss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');

var css = function(config) {
    var self = {};

    self.clean = function() {
        return del(config.dist.css);
    };

    self.build = function() {
        return gulp.src(config.source.css)
            .pipe(sass({
                style: 'compressed',
                loadPath: './node_modules'
            }).on('error', notify.onError(function (err) {
                return 'CSS Error:' + err.message;
            })))
            .pipe(autoprefixer('last 2 version'))
            .pipe(minifycss())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(config.dist.css))
    };

    self.cleanBuild = function() {
        self.clean();
        self.build();
    };

    self.watch = function() {
        return gulp.watch(config.watch.css, self.build);
    };

    return self;
};

module.exports = css;
