'use strict';

var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var inlineCss = require('gulp-inline-css');
var inlinesource = require('gulp-inline-source');
var minifycss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var replace = require('gulp-replace');

var inline = function(config) {
    var self = {};

    self.build = function() {
      if (config.email.production) {
          return gulp.src(config.dist.serve)
            .pipe(inlinesource())
            .pipe(replace('src="assets/img/', 'src="' + config.email.assetPath))
            .pipe(inlineCss({
              removeStyleTags: false
            }))
            .pipe(notify('Inlined Production HTML'))
            .pipe(gulp.dest(config.dist.html));
      } else {
          return gulp.src(config.dist.serve)
            .pipe(inlinesource())
            .pipe(inlineCss({
              removeStyleTags: false
            }))
            .pipe(notify('Inlined Local HTML'))
            .pipe(gulp.dest(config.dist.html));
      }
    };

    return self;
};

module.exports = inline;
