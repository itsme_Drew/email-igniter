'use strict';

var gulp = require('gulp');
var inject = require('gulp-inject');
var rename = require('gulp-rename');

var video = function(config) {
    var self = {};

    self.build = function(done) {
      if (config.email.video) {
        self.buildVideo();
      }
      done();
    };

    self.buildVideo = function() {
        return gulp.src(config.dist.root + '/*.html')
            .pipe(inject(gulp.src([config.source.partials + '/video/*.html']), {
              starttag: '<!-- {{name}}:video -->',
              endtag: '<!-- end {{name}}:video -->',
              transform: function (filePath, file) {
                return file.contents.toString('utf8')
              }
            }))
            .pipe(rename({ suffix: '-video' }))
            .pipe(gulp.dest(config.dist.html))
    };

    return self;
};

module.exports = video;
