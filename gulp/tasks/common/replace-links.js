'use strict';

var gulp = require('gulp');
var replace = require('gulp-replace');
var notify = require('gulp-notify');

var replaceLinks = function(config) {
    var self = {};

    self.build = function() {
      if (config.email.video) {
        return gulp.src(config.dist.root + '/**/*.html')
            .pipe(replace(config.email.trigger.remoteFile, config.email.remotePath + '-video.html'))
            .pipe(gulp.dest(config.dist.html))
      } else {
        return gulp.src(config.dist.root + '/**/*.html')
            .pipe(replace(config.email.trigger.remoteFile, config.email.remotePath + '.html'))
            .pipe(gulp.dest(config.dist.html))
      }
    };

    return self;
};

module.exports = replaceLinks;
