# Email Igniter v4.1
### Change Log
###### 4.1
* Finally updates all of the npm packages
* Fixes issue with gulp serving the correct html file
* Changes default production to false so images load correctly

### Client Support
Supports Outlooks 2010/2011/2013/2015, Gmail, Apple Mail, and for those who use Yahoo Mail.
##### Remember - Outlook 2010/2013 DOES NOT support display block or background images. There is a wrapper fix that forces an outside table with a set width of 600.
##### Outlook 2010/2013 only accepts the rule classes on tables. To fix that use the conditional code for the Border Fix included where you want the rule to show.

### Usage

* Run ```npm install```
* Config email options in gulp/tasks/app/config.js
* Run gulp serve to clean, build, watch
* Other gulp commands ```gulp clean:email``` ```gulp build:email```

### Features

* Replaces special characters with the character code. Add more in gulp/tasks/common/replace-characters
* Trigger words. Can be configured in gulp/tasks/app/config.js. In your html use a trigger word (eg: titleTrigger) to replace with the title in the gulp email config. *Want to build more trigger words?* Add to the config.email.trigger and add the replace in gulp/tasks/common/html.js
  1. titleTrigger - Replaces with the title
  2. clientTrigger - replaces with 'lc' and 'dt' and creates a new example-lc.html & example-dt.html file into dist. Config.lcdt must be set to true. eg: promo-customerTrigger.jpg will replace to promo-lc.jpg
  3. remoteFileTrigger - replaces with the remote file url built using email config. eg: ```<a href="remoteEmailTrigger"></a>``` becomes ```<a href="http://domain.com/promotions/email-starter-pack.html"></a>```
* Builds asset folder to copy to FTP
* Renames file to email name in dist folder
* Inlines CSS
* If config production is true - replaces all 'assets/img/' with domain.com/path/assets/img/
* Uses gulp 4.0
* If config video is true in email config - replaces anything between ```<!-- inject:video --> CODE <!-- end inject:video -->``` with src/partials/video/video.html and creates a email-starter-pack-video.html into dist. Ideal for having a gif image replaced with a video code.
